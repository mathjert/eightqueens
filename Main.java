package eightqueens;
import eightqueens.*;

public class Main {
    public static void main(String[] args) {
        long startTime = System.nanoTime();
        EightQueens eq = new EightQueens(0, 0, 8);
        long endTime   = System.nanoTime();
       long totalTime = (endTime - startTime)/1_000_000_000;
       System.out.println(totalTime);
    }
}