package eightqueens;

import java.util.HashMap;
import java.util.Map;

public class EightQueens {

    private int x;
    private int y;
    private int boardsize;
    HashMap<Integer, Integer> board = new HashMap<>();

    public EightQueens(int x, int y, int boardsize) {
        this.x = x;
        this.y = y;
        this.boardsize = boardsize;
        initializeBoard();
        int[] test = new int[boardsize];
        HashMap<Integer, Integer> res = findSolution2(x + 1, 1, board);
       for (int i = 0; i < boardsize; i++) {
            test[i] = res.get(i)+1;
        }
        writeBoard(test);

    }

    private void initializeBoard() {
        board.put(x, y);
    }

    public HashMap<Integer, Integer> findSolution2(int checkColumn, int depth, HashMap<Integer, Integer> boardState) {
        if (checkColumn == this.boardsize)
            checkColumn = 0;

        int prevStep = checkColumn == 0 ? this.boardsize-1 : checkColumn-1;
        for (int i = 0; i < boardsize; i++) {

            
            boolean cont = false;
            for (Map.Entry<Integer, Integer> entry : boardState.entrySet()) {
                int subtract = checkColumn - entry.getKey();
                if (i == (entry.getValue() -  subtract) || i == (entry.getValue() + subtract)){
                    cont = true;
                    break;
                }
            }
            if (cont)
                continue;
            
             
            if (boardState.containsValue(i))
                continue;
            if (depth == boardsize-1) {
                boardState.put(checkColumn, i);
                return boardState;
            }


            HashMap<Integer, Integer> checkList = new HashMap<>();
            for (Map.Entry<Integer, Integer> entry : boardState.entrySet()) {
                checkList.put(entry.getKey(), entry.getValue());
            }
            checkList.put(checkColumn, i);
            boardState.put(checkColumn, i);
            checkList = findSolution2(checkColumn + 1, depth + 1, checkList);
            if (!checkList.equals(boardState)){
               
                return checkList;
            }
            boardState.remove(checkColumn);
                
        }
        return boardState;
    }

    public void writeBoard(int[] boardGrid){
        String grid = "";
        for (int row : boardGrid){
            for (int i=1; i<row; i++){
                grid += " X ";
            }
            grid += " Q ";
            for (int i=0; i<(boardsize-row); i++){
                grid += " X ";
            }
            grid += "\r\n";
        }
        System.out.println(grid);
    }
}
